export interface Restaurant {
  uid: string;
  name: string;
  rating: number;
  avgRating: number;
  urlPicture: string;
  location: string;
  city: string;
  category: number;
}
