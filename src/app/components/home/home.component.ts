import {Component, OnInit} from '@angular/core';
import {Restaurant} from '../../interfaces/app.interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  restaurantList: Restaurant [] = [
    {
      uid: '1',
      name: 'Holy Moly',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/holy-moly.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Uncle Fletch',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/uncle-fletch.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Fuente Alemana',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/fuente-alemana.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'La Burguesia',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/la-burguesia.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Uncle Fletch Ñuñoa',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/uncle-fletch-nunoa.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Route 66',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/route-66.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Streat',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/streat.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Honesto Mike',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/honesto-mike.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Donde Guido',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/donde-guido.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'La Terraza',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/la-terraza.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    },
    {
      uid: '1',
      name: 'Juicy Lucy',
      rating: 5,
      avgRating: 4.9,
      urlPicture: 'assets/img/juicy-lucy.jpg',
      location: 'Serrano 299 Metro U. de Chile',
      city: 'santiago',
      category: 1
    }];

  constructor() {
  }

  ngOnInit(): void {

  }

}
